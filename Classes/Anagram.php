<?php

namespace Classes;

class Anagram
{
    use Formatter;

    private $string1, $string2;

    public function __construct($string1, $string2)
    {
        $this->string1 = $this->format_text($string1);
        $this->string2 = $this->format_text($string2);
    }

    public function is_anagram()
    {
        return count_chars($this->string1,1) == count_chars($this->string2,1);
    }
}