<?php

namespace Classes;

trait Formatter
{
    public function format_text($string)
    {
        return mb_strtolower(str_ireplace(' ', '', $string),'UTF-8');
    }
}